console.log('hello world')

// assignment operator ( = )
let assignmentNumber = 8;


// arithmetic operator ( + / * - %)

// addition assignement operator ( += )

assignmentNumber += 2; //answer is 10
console.log(assignmentNumber);

//subtraction assignment operator ( -= )
assignmentNumber -= 2; //answer is 8
console.log(assignmentNumber);

//multiplication/division assignmet operator ( *= /=)

assignmentNumber *= 2; //answer is 16
console.log(assignmentNumber);

assignmentNumber /= 2; //answer is 8
console.log(assignmentNumber);

//multiple operators and parenthesis

let mdas = 2 + 10 - 1 * 9 / 3;
console.log(mdas);

let pemdas = 1 + (2 - 1) * (5 / 5);
console.log(pemdas);

//increment/decrement operator

let z = 1;

//increment

//pre-fix incrementation
// the value of z is added with 1 before returning the value and storing the data

++z;
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

//post fix incrementation 
//the value of z is returned and stored in the variable then the value of z increased
z++;
console.log(z); //3 
console.log(z++); //3 the previous value is returned
console.log(z); //4 added with 1

console.log(++z); //5 the new value is returned immediately



//decrement

//post and pre decrement

console.log(--z); //4
console.log(z--); //4
console.log(z); //3

//type coercion

let numA = '12';
let numB = 10;

let coersion = numA + numB;

console.log(coersion); //1210
console.log(typeof coersion); //string

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number 


//comparison operators

// (==) equality operator

//check if the statement is equal
let juan;
console.log(1 == 1); //true
console.log(1 == 23); //false
console.log(1 == '1'); //true
console.log(0 == false) //true
console.log('juan' == "JUAN"); //false case sensitive
console.log('juan' == juan); //true

//( != ) Inequality operator

console.log(1 != 1); //false

//( === )strictly equality operator

console.log(1 === 1); //true
console.log(1 === 23); //false
console.log(1 === '1'); //false
console.log(0 === false) //false
console.log('juan' === "JUAN"); //false case sensitive
console.log('juan' === juan); //true

//( !== ) strict inequality operator
console.log(1 !== 1); //false

//Relational Comparison Operators
//check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("Greater than")
//Greater than (>)
console.log(x > y); //false
console.log(w > y); //true

console.log("Less than")
//Less than (<)
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion 
console.log(numString < 6000); //true - forced coercion to change string to number
console.log(numString < "Jose"); //true



//logical operators

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

//logical AND Operator (&& - double ampersand)

console.log("Logical AND Operator")

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log (authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false


let authorization4 = isRegistered && requiredLevel === 95;
console.log(authorization4); //true

let userName = "gamer2022";
let userName2 = "orl1234";
let userAge = "15";
let userAge2 = "30";

let registration1 = userName.length > 8 && userAge >= requiredAge;
// .length is a property of strings which determine the number of char in the string
console.log(registration1); //false

console.log("Logical OR Operator");
//OR operator (|| double pipe)

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;

console.log(guildRequirement1); //true

console.log("Not Operator");
//NOT Operator (!)
//turn boolean to opposite value

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true

//if, else if, else statement

/*if(true){
	alert("We just run an if condition")
}*/

let numG = -1;


if(numG < 1){
	console.log('hello');
}

let userName3 = "crusader_1993"
let userlevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Link Start");
}

if (userlevel3.length >= requiredLevel) {
	console.log("You are qualified to join the guild");
}

//else

if (userName3.length >= 10 && userlevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noobs Guild");
} else {
	console.log("you are too strong to be a noob.")
}

//else if

if (userName3.length >= 10 && userlevel3 <= 25 && userAge3 >= requiredAge) {
	console.log("Thank you for joining the Noobs Guild");
} else if (userlevel3 > 25) {
	console.log("you are too strong to be a noob.");
} else	if (userAge3 < requiredAge) {
	console.log("Too young to join us");
} else {
	console.log("better luck next time.");
}

//if, else if, else statement with functions

function addNum(num1, num2){
	if (typeof num1 === "number" && typeof num2 === "number") {
		console.log("run only if both arguments are number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers.");
	}
}

addNum(5, 2);

//create log in function

function login(username, password){
	if (typeof username === "string" && typeof password === "string") {
		console.log("both are string")

		if (password.length >= 8 && username.length >= 8) {
			 
				console.log("Thank you for logging in.");
			} else {
		 		
		 		if (password.length < 8 && username.length < 8) {
					alert("Credentials too short");

				} else 	if (username.length < 8) {
					alert("username too short");
				} else if (password.length < 8) {
					alert("password too short");
				}
			
		}
	} else {
		console.log("one is not a string");
	}
}

login("jhjhjfgjfjgf", "jajkghjkhgjhgn")

//function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windspeed){
	if (windspeed < 30) {
		return 'Not a typhoon yet';
	} else if (windspeed <= 61) {
		return 'Tropical depression detected';
	} else if (windspeed >= 62 && windspeed <= 88) {
		return 'Tropical storm detected.';
	}else if (windspeed >= 89 && windspeed <= 117) {
		return 'Severe tropical storm detected';
	}else {
		return 'Typhoon detected';
	}

}

message = determineTyphoonIntensity(68);
console.log(message);
//console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

if (message == 'Tropical storm detected.') {
	console.warn(message);
}

//truthy and falsy
if (true) {
	console.log('truthy');
}

if(1) {
	console.log('true');
}

if ([]) {
	console.log('truthy')
}



//falsy
//-0, "", null, NaN
if (false) {

	console.log('falsy')
}

//Ternary Operator

/*
Syntax;
	(expression/condition) ? ifTrue : ifFalse;
three operands of ternary operation:
1. Condition 
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy
*/

let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of ternary operator: ${ternaryResult}`);

//if using if statement

if (1<18) {
	console.log(true);
} else {
	console.log(false);
}


let price = 5000;

price > 1000 ? console.log('price is over 1000') : console.log('price is below 1000');

//ternary operators have an implicit return statement that without return keyword, 

//else if ternary operator

let a = 7;

a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));

//multiple statement execution

let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit'
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit";
}
/*
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`);*/

//Mini - activity

let date = prompt("What day is today?");


if (typeof date === 'string') {

	let dateToday = date.toLowerCase();

	function dayToday(day){
		return day;
	}

	function colorOfTheDay(){
		if (dateToday == 'monday') {
			return 'Black';

		} else if (dateToday == 'tuesday') {
			return 'Green';
		} else if (dateToday == 'wednesday') {
			return 'Yellow';
		} else if (dateToday == "thursday") {
			return 'Red';
		} else if (dateToday == 'friday') {
			return 'Violet';
		} else if (dateToday == 'saturday') {
			return 'Blue';
		} else if (dateToday == 'sunday') {
			return 'White';
		}
	}

		let day = dayToday(dateToday);
		let color = colorOfTheDay();
		day == 'monday' && 'tuesday' && 'wednesday' && 'thursday' && 'friday' && 'saturday' && 'sunday'
		? alert(`Today is ${day}, Wear ${color}`)
		: alert("Invalid Input. Enter a valid day of the week");
	} else {
		alert("Invalid input. Please input a string");
	}

//switch statement

/*
syntax: 
	switch (expression/condition){
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
*/

let hero = prompt("Enter a Name").toLowerCase();

switch (hero) {
	case "jose rizal":
		console.log("National Hero of the Philippines");
		break;
	case "george washington":
		console.log("Hero of the American Revolution");
		break;
	case "hercules":
		console.log("Legendary Hero of the Greek");
		break;
	default:
		console.log("please type again")
}


//Try-Catch-Finally Statement
//this is used for error handling


function showIntensityAlert(windSpeed) {
	//Attempt to execute a code 
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}catch (error) {
		//error/err are commonly used variable by developers for storing errors
		console.log(typeof error);

		//Catch errors within 'try' statement
		//the "error.message" is used to access the information relating to an error object
		console.warn(error.message)
	}
	finally {
		//Continue execution of code REGARDLESS of success or failure of code execution in the 'try' block to handle/resolve errors.
		//optional
		alert('Intensity updates will show new alert')
	}

}


showIntensityAlert(68);

//throw - user-defined exception
//Execution of the current function will stop


const number = 40;

try {
	if(number > 50) {
		console.log('Success');
	} else {
		//user-defined throw statement
		throw Error('The number is low');
	}

	//if throw executes, the below code does not execute
	console.log('hello')
}
catch(error) {
	console.log('An error caugth');
	console.warn(error.message)
}
finally {
	console.log("Please add a higher number")
}


//another example

function getArea(width, height) {
	if(isNaN(width) || isNaN(height)) {
		throw 'Parameter is not a number!'
	}
}

try {
	getArea(3, 'A');
}
catch(e) {
	console.error(e)
}
finally {
	alert("Number only")
}


